INSERT INTO category (description) VALUES ('Thai');
INSERT INTO category (description) VALUES ('Chinese');
INSERT INTO category (description) VALUES ('Japanese');
INSERT INTO category (description) VALUES ('Indian');
INSERT INTO category (description) VALUES ('American');
INSERT INTO category (description) VALUES ('Mexican');

INSERT INTO unit_of_measure (description) VALUES ('Milliliter');
INSERT INTO unit_of_measure (description) VALUES ('Gram');
INSERT INTO unit_of_measure (description) VALUES ('Cup');
INSERT INTO unit_of_measure (description) VALUES ('Teaspoon');
INSERT INTO unit_of_measure (description) VALUES ('Tablespoon');
INSERT INTO unit_of_measure (description) VALUES ('Ounce');
INSERT INTO unit_of_measure (description) VALUES ('Each');
INSERT INTO unit_of_measure (description) VALUES ('Dash');
INSERT INTO unit_of_measure (description) VALUES ('Pint');