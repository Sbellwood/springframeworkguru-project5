package com.sfg.springframeworkguruproject5.enums;

public enum Difficulty {
    EASY,
    MEDIUM,
    HARD
}
