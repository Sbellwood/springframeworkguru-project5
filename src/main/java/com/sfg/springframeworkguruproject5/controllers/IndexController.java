package com.sfg.springframeworkguruproject5.controllers;

import com.sfg.springframeworkguruproject5.models.Category;
import com.sfg.springframeworkguruproject5.models.UnitOfMeasure;
import com.sfg.springframeworkguruproject5.repositories.CategoryRepository;
import com.sfg.springframeworkguruproject5.repositories.RecipeRepository;
import com.sfg.springframeworkguruproject5.repositories.UnitOfMeasureRepository;
import com.sfg.springframeworkguruproject5.services.RecipeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
public class IndexController {

    private final CategoryRepository categoryRepository;
    private final UnitOfMeasureRepository unitOfMeasureRepository;
    private final RecipeService recipeService;

    public IndexController(CategoryRepository categoryRepository, UnitOfMeasureRepository unitOfMeasureRepository, RecipeService recipeService) {
        this.categoryRepository = categoryRepository;
        this.unitOfMeasureRepository = unitOfMeasureRepository;
        this.recipeService = recipeService;
    }

    @RequestMapping({"","/", "/index"})
    public String getIndexPage(Model model) {

        Optional<Category> category = categoryRepository.findByDescription("Thai");
        Optional<UnitOfMeasure> unitOfMeasure = unitOfMeasureRepository.findByDescription("Gram");

        System.out.println(category.get().getId() + " " + category.get().getDescription());
        System.out.println(unitOfMeasure.get().getId() + " " + unitOfMeasure.get().getDescription());

        model.addAttribute("recipes", recipeService.getRecipes());

        return "index";
    }
}
