package com.sfg.springframeworkguruproject5.services;

import com.sfg.springframeworkguruproject5.models.Recipe;

import java.util.Set;

public interface RecipeService {

    Set<Recipe> getRecipes();
}
