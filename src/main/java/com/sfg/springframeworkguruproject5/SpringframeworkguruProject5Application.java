package com.sfg.springframeworkguruproject5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringframeworkguruProject5Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringframeworkguruProject5Application.class, args);
    }

}
