package com.sfg.springframeworkguruproject5.repositories;

import com.sfg.springframeworkguruproject5.models.Recipe;
import org.springframework.data.repository.CrudRepository;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {
}
